# Introduction
This is a simple application (NYC Schools) that consumes some data from the NY school API here https://dev.socrata.com/foundry/data.cityofnewyork.us/s3k6-pzi2 it also fetches SAT average scores from here https://dev.socrata.com/foundry/data.cityofnewyork.us/f9bf-2cp4
There's currently only 2 simple screen, a list of all schools (names and address), and a score details screen

## Structure
The app is constructed completely in Jetpack Compose using MVVM architecture. The view model is the bridge between the network layer and the view layer, handling state business logic as well. (i.e. fetching, error, success)

### Tools/Frameworks and dependencies:
- [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)
- [Flows](https://developer.android.com/kotlin/flow)
- [Navigation with Jetpack compose](https://developer.android.com/jetpack/compose/navigation)
- [HILT](https://developer.android.com/training/dependency-injection/hilt-android) Dependency Injection Framework
- [Moshi](https://github.com/square/moshi) JSON parser
- [Retrofit](https://github.com/square/retrofit) HTTP client
- [Jetpack Compose](https://developer.android.com/jetpack/compose).
- [Timber](https://github.com/JakeWharton/timber) for logging.

### Current Design and Architecture
Since it's a pretty simple app, all code is contained in one module, but everything is organized in packages. For example, the common package contains all the generic and default implementations that all other modules need and can extend, which includes things like:
* Network: Which can provide things like http client, json parsers .. etc.
* Coroutines: Which provides things like coroutine dispatchers.
* UI components such as Themes, Colors, Typography and some stateless reusable UI components
* Logger interface
* Extensions