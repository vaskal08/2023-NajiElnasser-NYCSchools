package com.example.nycschools

import com.example.nycschools.common.model.UIModel
import com.example.nycschools.data.repositories.SchoolsDataSource
import com.example.nycschools.data.repositories.SchoolsRepository
import com.example.nycschools.data.repositories.schools.model.NYCSchoolsResponseItem
import com.example.nycschools.common.logger.Logger
import com.example.nycschools.ui.schools.SchoolsViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolsTest {
    private val schoolsRemoteDataSource = mockk<SchoolsDataSource>()

    private val logger = mockk<Logger>(relaxed = true)

    private val schoolsRepository = SchoolsRepository(schoolsRemoteDataSource)

    private val dispatcher = StandardTestDispatcher()

    private val schoolsViewModel by lazy {
        SchoolsViewModel(
            schoolsRepository,
            dispatcher,
            logger
        )
    }

    @Before
    fun setup() {
        coEvery {
            schoolsRemoteDataSource.getAllSchools()
        } returns flowOf(Result.success(listOf()))

        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test initial state`() = runTest {
        val state = schoolsViewModel.state.value
        assert(state == null)
    }

    @Test
    fun `test error state`() = runTest {
        val exception = Throwable("oops!")

        coEvery {
            schoolsRemoteDataSource.getAllSchools()
        } returns flowOf(Result.failure(exception))

        val state = schoolsViewModel.state.value

        assert(state == null)

        advanceUntilIdle()

        val errorState = schoolsViewModel.state.value

        assert(errorState is UIModel.Error)

        assert((errorState as UIModel.Error).e.message == exception.message)
    }

    @Test
    fun `test successful empty state`() = runTest {
        coEvery {
            schoolsRemoteDataSource.getAllSchools()
        } returns flowOf(Result.success(listOf()))

        val state = schoolsViewModel.state.value

        assert(state == null)

        advanceUntilIdle()

        val successState = schoolsViewModel.state.value

        assert(successState is UIModel.Success)

        assert((successState as UIModel.Success).data != null)

        val schools = successState.data as List<NYCSchoolsResponseItem>

        assert(schools.isEmpty())
    }

    @Test
    fun `test successful state with data`() = runTest {
        coEvery {
            schoolsRemoteDataSource.getAllSchools()
        } returns flowOf(Result.success(mockedSchoolsData))

        val state = schoolsViewModel.state.value

        assert(state == null)

        advanceUntilIdle()

        val successState = schoolsViewModel.state.value

        assert(successState is UIModel.Success)

        assert((successState as UIModel.Success).data != null)

        val schools = successState.data as List<NYCSchoolsResponseItem>

        assert(schools.isNotEmpty())

        assert(schools.size == mockedSchoolsData.size)

        assert(schools == mockedSchoolsData)
    }
}

private val mockedSchoolsData by lazy {
    listOf(
        NYCSchoolsResponseItem(
            dbn = "29Q272",
            schoolName = "Clinton School Writers & Artists"
        ),
        NYCSchoolsResponseItem(
            dbn = "21K728",
            schoolName = "Liberation Diploma Plus High School"
        )
    )
}