package com.example.nycschools

import android.app.Application
import com.example.nycschools.common.logger.Logger
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class NYCSchoolsApp : Application() {
    /**
     * Instance of [Logger]
     * */
    @Inject
    lateinit var logger: Logger

    override fun onCreate() {
        super.onCreate()
        initLogger()
    }

    private fun initLogger() {
        logger.prepare(BuildConfig.DEBUG)
    }
}