package com.example.nycschools.data.repositories

import com.example.nycschools.data.repositories.schools.model.NYCSchoolsResponseItem
import com.example.nycschools.data.repositories.schools.model.SATScoresResponseItem
import kotlinx.coroutines.flow.Flow

interface SchoolsDataSource {

    suspend fun getAllSchools(): Flow<Result<List<NYCSchoolsResponseItem>>>

    suspend fun getSATData(dbn: String): Flow<Result<List<SATScoresResponseItem>>>
}