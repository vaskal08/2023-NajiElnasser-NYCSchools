package com.example.nycschools.data.repositories.schools.model

import com.squareup.moshi.Json

data class SATScoresResponseItem(

	@Json(name="dbn")
	val dbn: String? = null,

	@Json(name="sat_writing_avg_score")
	val satWritingAvgScore: String? = null,

	@Json(name="sat_critical_reading_avg_score")
	val satCriticalReadingAvgScore: String? = null,

	@Json(name="sat_math_avg_score")
	val satMathAvgScore: String? = null,

	@Json(name="school_name")
	val schoolName: String? = null,

	@Json(name="num_of_sat_test_takers")
	val numOfSatTestTakers: String? = null
)
