package com.example.nycschools.data.repositories

import javax.inject.Inject

class SchoolsRepository @Inject constructor(
    private val schoolsRemoteDataSource: SchoolsDataSource
) {
    suspend fun getAllSchools() = schoolsRemoteDataSource.getAllSchools()

    suspend fun getSATScores(dbn: String) = schoolsRemoteDataSource.getSATData(dbn)
}