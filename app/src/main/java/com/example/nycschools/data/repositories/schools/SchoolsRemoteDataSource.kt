package com.example.nycschools.data.repositories.schools

import android.content.Context
import com.example.nycschools.common.asNetworkAwareFlow
import com.example.nycschools.data.repositories.SchoolsDataSource
import com.example.nycschools.service.schools.SchoolsApi
import com.example.nycschools.common.toResult
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SchoolsRemoteDataSource @Inject constructor(
    private val schoolsApi: SchoolsApi,
    @ApplicationContext private val context: Context
) : SchoolsDataSource {

    override suspend fun getAllSchools() = flow {
        emit(schoolsApi.fetchAllSchools().toResult())
    }.asNetworkAwareFlow(context)

    override suspend fun getSATData(dbn: String) = flow {
        emit(schoolsApi.fetchSATScores(dbn).toResult())
    }.asNetworkAwareFlow(context)
}