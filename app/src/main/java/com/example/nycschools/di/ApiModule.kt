package com.example.nycschools.di

import com.example.nycschools.BuildConfig
import com.example.nycschools.service.schools.SchoolsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.create
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ApiModule {
    /**
     * Function to provide [SchoolsApi]
     * @return [SchoolsApi]
     * */
    @Singleton
    @Provides
    fun provideCityOfNewYorkApi(
        retrofitBuilder: Retrofit.Builder,
        httpClient: OkHttpClient.Builder
    ): SchoolsApi = retrofitBuilder.baseUrl(BuildConfig.API_BASE_URL)
        .client(httpClient.build())
        .build()
        .create()
}