package com.example.nycschools.di

import android.content.Context
import com.example.nycschools.data.repositories.SchoolsDataSource
import com.example.nycschools.data.repositories.schools.SchoolsRemoteDataSource
import com.example.nycschools.service.schools.SchoolsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object AppModule {
    @Singleton
    @Provides
    fun provideSchoolsRemoteDataSource(
        schoolsApi: SchoolsApi,
        @ApplicationContext context: Context
    ): SchoolsDataSource = SchoolsRemoteDataSource(
        schoolsApi = schoolsApi,
        context = context
    )
}