package com.example.nycschools.service.schools

import com.example.nycschools.data.repositories.schools.model.NYCSchoolsResponseItem
import com.example.nycschools.data.repositories.schools.model.SATScoresResponseItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApi {
    @GET("s3k6-pzi2.json")
    suspend fun fetchAllSchools(): Response<List<NYCSchoolsResponseItem>>

    @GET("f9bf-2cp4.json")
    suspend fun fetchSATScores(
        @Query("dbn") dbn: String
    ): Response<List<SATScoresResponseItem>>
}