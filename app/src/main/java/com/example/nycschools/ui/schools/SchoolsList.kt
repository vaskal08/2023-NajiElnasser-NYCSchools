package com.example.nycschools.ui.schools

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.nycschools.R
import com.example.nycschools.common.exception.APIException
import com.example.nycschools.common.exception.NetworkUnavailableException
import com.example.nycschools.common.exception.NotFoundException
import com.example.nycschools.common.model.UIModel
import com.example.nycschools.common.ui.CircularProgressIndicator
import com.example.nycschools.common.ui.ErrorText
import com.example.nycschools.common.ui.LineDivider
import com.example.nycschools.data.repositories.schools.model.NYCSchoolsResponseItem

@Composable
fun Schools(
    schoolsViewModel: SchoolsViewModel = hiltViewModel(),
    onSchoolClick: (String?) -> Unit
) {
    when (val state = schoolsViewModel.state.collectAsState().value) {
        is UIModel.Success -> {
            val schools = (state.data as List<NYCSchoolsResponseItem?>)
            NYCSchoolsList(
                schools = schools,
                onSchoolClick = onSchoolClick
            )
        }
        is UIModel.Error -> {
            val errorMessage = when (val error = state.e) {
                is NetworkUnavailableException -> stringResource(id = R.string.error_no_network)
                is NotFoundException -> stringResource(id = R.string.error_empty_details)
                is APIException -> error.errorBody?: stringResource(id = R.string.error)
                else -> stringResource(id = R.string.error)
            }
            ErrorText(text = errorMessage)
        }
        is UIModel.Loading -> {
            CircularProgressIndicator()
        }
        else -> {

        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun NYCSchoolsList(
    schools: List<NYCSchoolsResponseItem?>,
    onSchoolClick: (String?) -> Unit
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.secondary)
    ) {
        items(schools) { item ->
            SchoolRow(
                school = item,
                onSchoolClick = onSchoolClick
            )

            LineDivider()
        }
    }
}

@Composable
private fun LazyItemScope.SchoolRow(
    school: NYCSchoolsResponseItem?,
    onSchoolClick: (String?) -> Unit
) {
    Column(
        modifier = Modifier
            .padding(10.dp)
            .clickable { onSchoolClick(school?.dbn) }
    ) {
        Text(
            text = school?.schoolName ?: "",
            fontWeight = FontWeight.Bold
        )

        Spacer(modifier = Modifier.height(5.dp))

        Text(
            fontSize = 11.sp,
            text = school?.primaryAddressLine1 ?: ""
        )

        Spacer(modifier = Modifier.height(2.dp))

        Text(
            fontSize = 11.sp,
            text = "${school?.city?: ""}, ${school?.stateCode?: ""}"
        )
    }
}