package com.example.nycschools.ui.schools

import androidx.compose.runtime.Composable
import androidx.navigation.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable

const val ROUTE_HOME = "home"
const val SCHOOL_ID = "schoolId"
const val ROUTE_SCHOOL_DETAIL = "details/{$SCHOOL_ID}"

@Composable
fun SchoolsNavGraph(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = ROUTE_HOME
    ) {
        addSchools(navController)
        addSchoolDetail()
    }
}

fun NavGraphBuilder.addSchools(
    navController: NavController
) {
    composable(
        route = ROUTE_HOME
    ) {
        Schools(
            onSchoolClick = {
                it?.let {
                    navController.navigate("details/$it")
                }
            }
        )
    }
}

fun NavGraphBuilder.addSchoolDetail() {
    composable(
        route = ROUTE_SCHOOL_DETAIL,
        arguments = listOf(
            navArgument(name = SCHOOL_ID) {
                type = NavType.StringType
                defaultValue = ""
            }
        )
    ) {
        val id = it.arguments?.getString(SCHOOL_ID)?: ""
        SchoolDetail(dbn = id)
    }
}

fun String.isHomeScreen() = this == ROUTE_HOME