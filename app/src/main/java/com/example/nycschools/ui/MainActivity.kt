package com.example.nycschools.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.nycschools.R
import com.example.nycschools.common.ui.TopBarWithBackArrow
import com.example.nycschools.common.ui.theme.NYCSchoolsTheme
import com.example.nycschools.ui.schools.*
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NYCSchoolsTheme {
                Surface(
                    modifier = Modifier.fillMaxSize()
                ) {
                    val navController = rememberNavController()

                    Column {
                        TopBar(navController)
                        SchoolsNavGraph(navController)
                    }
                }
            }
        }
    }
}

@Composable
fun TopBar(navController: NavController) {
    val currentRoute = navController.currentBackStackEntryAsState().value?.destination?.route
    val title = stringResource(id = if (currentRoute?.isHomeScreen() == true) R.string.nyc_schools_title else R.string.sat_scores_title)
    val showBackArrow = currentRoute?.isHomeScreen() == false

    TopBarWithBackArrow(
        showBackArrow = showBackArrow,
        title = title,
        onBackPressed = {
            navController.popBackStack()
        }
    )
}