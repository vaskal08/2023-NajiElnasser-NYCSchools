package com.example.nycschools.ui.schools

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.common.model.UIModel
import com.example.nycschools.data.repositories.SchoolsRepository
import com.example.nycschools.common.di.IODispatcher
import com.example.nycschools.common.exception.toError
import com.example.nycschools.common.logger.Logger
import com.example.nycschools.common.result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolsViewModel @Inject constructor(
    private val repository: SchoolsRepository,
    @IODispatcher private val ioDispatcher: CoroutineDispatcher,
    private val logger: Logger
) : ViewModel() {
    /**
     * A mutable UI Model state observable
     */
    private val _state = MutableStateFlow<UIModel<*>?>(null)

    /**
     * An immutable UI Model state observable
     */
    val state: StateFlow<UIModel<*>?>
        get() = _state

    init {
        fetchAllSchools()
    }

    private fun fetchAllSchools() {
        viewModelScope.launch {
            repository.getAllSchools()
                .map { it.result() }
                .flowOn(ioDispatcher)
                .catch {
                    logger.e(it, "error while fetching schools")
                    _state.value = UIModel.Error(it.toError())
                }.onStart { _state.value = UIModel.Loading }
                .collectLatest {
                    logger.d("received schools response= [$it]")
                    _state.value = UIModel.Success(it)
                }
        }
    }
}