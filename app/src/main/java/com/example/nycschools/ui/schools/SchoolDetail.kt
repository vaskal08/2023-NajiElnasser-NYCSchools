package com.example.nycschools.ui.schools

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.nycschools.R
import com.example.nycschools.common.exception.APIException
import com.example.nycschools.common.exception.NetworkUnavailableException
import com.example.nycschools.common.exception.NotFoundException
import com.example.nycschools.common.model.UIModel
import com.example.nycschools.common.ui.CircularProgressIndicator
import com.example.nycschools.common.ui.ErrorText
import com.example.nycschools.data.repositories.schools.model.SATScoresResponseItem

@Composable
fun SchoolDetail(
    dbn: String,
    schoolDetailViewModel: SchoolDetailViewModel = hiltViewModel()
) {
    when (val state = schoolDetailViewModel.state.collectAsState().value) {
        is UIModel.Success -> {
            val scoresMap = state.data as? Map<String?, SATScoresResponseItem?>
            val scoresForSchool = scoresMap?.get(dbn)

            if (scoresForSchool == null) {
                SATScoresEmptyView()
            } else {
                Card(
                    modifier = Modifier
                        .fillMaxSize()
                        .wrapContentSize(Alignment.Center)
                        .padding(10.dp)
                        .background(MaterialTheme.colorScheme.secondary),
                    shape = RoundedCornerShape(8.dp)
                ) {
                    SATScores(satScores = scoresForSchool)
                }
            }


        }
        is UIModel.Error -> {
            val errorMessage = when (val error = state.e) {
                is NetworkUnavailableException -> stringResource(id = R.string.error_no_network)
                is NotFoundException -> stringResource(id = R.string.error_empty_details)
                is APIException -> error.errorBody ?: stringResource(id = R.string.error)
                else -> stringResource(id = R.string.error)
            }
            ErrorText(text = errorMessage)
        }
        is UIModel.Loading -> {
            CircularProgressIndicator()
        }
        else -> {

        }
    }

    LaunchedEffect(Unit) {
        schoolDetailViewModel.fetchSATScores(dbn)
    }
}

@Composable
private fun SATScoresEmptyView() {
    Text(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
            .padding(20.dp),
        text = stringResource(id = R.string.error_empty_details),
        fontSize = 17.sp,
        textAlign = TextAlign.Center,
        fontFamily = FontFamily.SansSerif,
    )
}

@Composable
private fun SATScoresTitle(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 10.dp, end = 10.dp)
            .background(Color.White)
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(Alignment.CenterHorizontally)
                .padding(top = 20.dp, bottom = 10.dp),
            fontSize = 28.sp,
            fontWeight = FontWeight.Bold,
            fontFamily = FontFamily.SansSerif,
            text = stringResource(id = R.string.sat_scores_title)
        )

        Spacer(modifier = Modifier.height(10.dp))

        content()
    }
}

@Composable
private fun SATScores(satScores: SATScoresResponseItem) {
    Column(
        modifier = Modifier
            .wrapContentSize(Alignment.Center)
            .background(MaterialTheme.colorScheme.secondary)
            .padding(3.dp)
            .border(
                border = BorderStroke(1.dp, MaterialTheme.colorScheme.primary),
                shape = RoundedCornerShape(4.dp)
            )
            .padding(20.dp)
    ) {

        Text(
            text = satScores.schoolName?: "",
            fontWeight = FontWeight.Bold,
            fontSize = 22.sp
        )


        Spacer(modifier = Modifier.height(25.dp))

        ItemRow(
            title = stringResource(id = R.string.sat_math),
            value = satScores.satMathAvgScore ?: ""
        )

        Spacer(modifier = Modifier.height(10.dp))

        ItemRow(
            title = stringResource(id = R.string.sat_reading),
            value = satScores.satCriticalReadingAvgScore ?: ""
        )

        Spacer(modifier = Modifier.height(10.dp))

        ItemRow(
            title = stringResource(id = R.string.sat_writing),
            value = satScores.satWritingAvgScore ?: ""
        )
    }
}

@Composable
private fun ColumnScope.ItemRow(
    title: String,
    value: String
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = title,
            fontWeight = FontWeight.Bold
        )

        Text(
            text = " $value",
            fontSize = 13.sp
        )
    }
}