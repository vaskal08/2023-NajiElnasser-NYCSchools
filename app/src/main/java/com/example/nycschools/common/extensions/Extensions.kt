package com.example.nycschools.common

import android.content.Context
import com.example.nycschools.common.exception.APIException
import com.example.nycschools.common.exception.NetworkUnavailableException
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

/**
 * Transforms a Retrofit Response object to a data holder Result object
 * */
@Suppress("NestedBlockDepth")
fun <T> Response<T>.toResult(): Result<T> {
    return try {
        if (isSuccessful) {
            val body = body()
            if (body != null) {
                Result.success(body)
            } else Result.failure(Throwable("Empty body"))
        } else Result.failure(APIException(errorBody()?.string(), code()))
    } catch (e: IllegalArgumentException) {
        Result.failure(e)
    }
}

/**
 * Transforms a data holder Result object to T on success, or cancels
 * the stream on failure.
 * */
suspend fun <T> Result<T?>.result(fallbackErrorMessage: String = ""): T {
    return suspendCancellableCoroutine { continuation ->
        onSuccess {
            if (it != null) {
                continuation.resume(it)
            } else continuation.resumeWithException(CancellationException(fallbackErrorMessage))
        }

        onFailure {
            continuation.resumeWithException(it)
        }
    }
}

/**
 * Transforms a flow to handle network related exceptions
 * and propagates it down the stream
 * */
fun <T> Flow<T>.asNetworkAwareFlow(context: Context): Flow<T> {
    return catch { e ->
        throw if (context.hasNetwork()) e
        else NetworkUnavailableException()
    }
}