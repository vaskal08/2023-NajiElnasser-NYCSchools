package com.example.nycschools.common.di

import com.example.nycschools.BuildConfig
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {
    private const val TIMEOUT = 10L

    /**
     * Function to provide [HttpLoggingInterceptor]
     * */
    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    /**
     * Function to provide [OkHttpClient.Builder]
     * */
    @Singleton
    @Provides
    fun provideHttpClientBuilder(
        interceptor: HttpLoggingInterceptor,
        @Named("accessTokenHeaderInterceptor") accessTokenHeaderInterceptor: Interceptor
    ): OkHttpClient.Builder {
        val clientBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) { clientBuilder.addInterceptor(interceptor) }
        clientBuilder.callTimeout(TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(accessTokenHeaderInterceptor)
        return clientBuilder
    }

    /**
     * Function to provide [Interceptor] for adding token key header
     * */
    @Named("accessTokenHeaderInterceptor")
    @Provides
    @Singleton
    fun provideSubscriptionKeyHeaderInterceptor(): Interceptor {
        return Interceptor {
            val requestBuilder = it.request().newBuilder()
            requestBuilder.addHeader("X-App-Token", BuildConfig.APP_TOKEN_KEY)
            it.proceed(requestBuilder.build())
        }
    }

    /**
     * Function to provide [Moshi]
     * */
    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory())
        .build()

    /**
     * Function to provide [MoshiConverterFactory]
     * */
    @Provides
    @Singleton
    fun provideMoshiConvertorFactory(moshi: Moshi): MoshiConverterFactory =
        MoshiConverterFactory.create(moshi)

    /**
     * Function to provide [Retrofit.Builder]
     * */
    @Singleton
    @Provides
    fun provideRetrofitBuilder(moshi: Moshi): Retrofit.Builder =
        Retrofit.Builder().addConverterFactory(MoshiConverterFactory.create(moshi))
}