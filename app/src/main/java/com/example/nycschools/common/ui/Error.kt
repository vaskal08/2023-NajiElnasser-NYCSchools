package com.example.nycschools.common.ui

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ErrorText(text: String) {
    Text(
        modifier = Modifier.fillMaxSize()
            .wrapContentSize(Alignment.Center)
            .padding(20.dp),
        text = text,
        fontSize = 14.sp,
        textAlign = TextAlign.Center,
        fontFamily = FontFamily.SansSerif,
        color = Color.Red
    )
}