package com.example.nycschools.common.model

/**
 * Class to reflect UI state
 * */
sealed class UIModel<T> {
    /** Class for Loading State */
    object Loading : UIModel<Nothing>()

    /** Class for Error State
     * @param e
     * */
    data class Error(val e: Throwable) : UIModel<Any>()

    /**
     * Class for Success State
     * @param data of type [T]
     * */
    data class Success<T>(val data: T) : UIModel<T>()
}
