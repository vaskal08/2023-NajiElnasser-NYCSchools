package com.example.nycschools.common.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Qualifier

/**
 * Dependencies for Coroutines.
 * */
@InstallIn(SingletonComponent::class)
@Module
object CoroutinesModule {

    /**
     * Function to provide [Dispatchers.Default]
     * */
    @DefaultDispatcher
    @Provides
    fun bindsDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

    /**
     * Function to provide [Dispatchers.IO]
     * */
    @IODispatcher
    @Provides
    fun bindsIODispatcher(): CoroutineDispatcher = Dispatchers.IO

    /**
     * Function to provide [Dispatchers.Main]
     * */
    @MainDispatcher
    @Provides
    fun bindsMainDispatcher(): CoroutineDispatcher = Dispatchers.Main
}

/**
 * [IODispatcher]
 * */
@Qualifier
annotation class IODispatcher

/**
 * [MainDispatcher]
 * */
@Qualifier
annotation class MainDispatcher

/**
 * [DefaultDispatcher]
 * */
@Qualifier
annotation class DefaultDispatcher