package com.example.nycschools.common.di

import com.example.nycschools.common.logger.Logger
import com.example.nycschools.logger.NYCSchoolsLogger
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class BaseModule {

    @Singleton
    @Binds
    abstract fun bindsLogger(logger: NYCSchoolsLogger): Logger
}