package com.example.nycschools.common.ui

import androidx.compose.material3.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

/**
 * Composable function for a line divider
 */
@Composable
fun LineDivider() {
    Divider(
        color = Color.LightGray,
        thickness = 1.dp,
    )
}