package com.example.nycschools.common.exception

/**
 * A [Throwable] implementation for network unavailable
 * @property message
 * */
class NetworkUnavailableException(message: String? = null) : Throwable(message)

/**
 * A [Throwable] implementation that holds both error and code
 * @property errorBody
 * @property code
 * */
data class APIException(val errorBody: String?, val code: Int) : Throwable(errorBody)

/**
 * A [Throwable] implementation for any 404 error code
 * @property message
 * */
class NotFoundException(message: String? = null) : Throwable(message)

fun Throwable.toError(): Throwable {
    return when (this) {
        is APIException -> {
            if (code == 404) {
                NotFoundException()
            } else this
        }
        else -> this
    }
}